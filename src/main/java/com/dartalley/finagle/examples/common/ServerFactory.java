package com.dartalley.finagle.examples.common;

import java.net.InetSocketAddress;

import com.twitter.finagle.Service;
import com.twitter.finagle.builder.Server;
import com.twitter.finagle.builder.ServerBuilder;
import com.twitter.finagle.http.Http;
import com.twitter.finagle.http.Request;
import com.twitter.finagle.http.Response;
import com.twitter.finagle.http.RichHttp;

public class ServerFactory {
	public static Server getRichHttpServer(String name, Service<Request, Response> service) {
		// Start the server
		return ServerBuilder.safeBuild(
				service,
				ServerBuilder.get()
				.codec(new RichHttp<Request>(Http.get()))
				.name(name)
				.bindTo(new InetSocketAddress("localhost", 8080)));
	}
}
