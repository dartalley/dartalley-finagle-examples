package com.dartalley.finagle.examples.common;

import java.net.InetSocketAddress;

import org.jboss.netty.handler.codec.http.HttpRequest;
import org.jboss.netty.handler.codec.http.HttpResponse;

import com.twitter.finagle.Service;
import com.twitter.finagle.builder.ClientBuilder;
import com.twitter.finagle.http.Http;

public class ClientFactory {
	public static Service<HttpRequest, HttpResponse> getClient() {
		return ClientBuilder.safeBuild(ClientBuilder.get()
				.codec(Http.get()).hosts(new InetSocketAddress("localhost", 8080)).daemon(true)
				.hostConnectionLimit(10)); 
	}
}
