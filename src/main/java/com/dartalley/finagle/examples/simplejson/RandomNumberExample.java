package com.dartalley.finagle.examples.simplejson;

import java.util.Date;
import java.util.List;

import javax.ws.rs.core.UriBuilder;

import com.dartalley.common.server.finagle.FinagleServerBuilder;
import com.dartalley.common.server.finagle.ServiceBuilder;
import com.dartalley.finagle.clients.FinagleClient;
import com.dartalley.finagle.clients.FinagleClientBuilder;
import com.dartalley.finagle.examples.simplejson.RandomNumberHandler.RandomNumberRequest;
import com.dartalley.finagle.examples.simplejson.RandomNumberHandler.RandomNumberResponse;
import com.dartalley.finagle.router.EndPoints;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.collect.Lists;
import com.twitter.finagle.builder.Server;
import com.twitter.util.Await;
import com.twitter.util.Duration;
import com.twitter.util.Future;

public class RandomNumberExample {
	public static void main(String[] args) throws JsonProcessingException, Exception {
		// Experiment changing the number of threads on the service builder or using
		// a custom client builder.
		
		// Build Server - 
		ServiceBuilder sb = ServiceBuilder.create();
		Server server = FinagleServerBuilder.create()
			.bind(EndPoints.get("/generate").build(), sb.json(new RandomNumberHandler(), RandomNumberRequest.class))
					.build();
		
		// Build Client
		FinagleClient client = FinagleClientBuilder.create().build();
		List<Future<RandomNumberResponse>> futures = Lists.newArrayList();
		long start = new Date().getTime();
		for (int i = 0; i < 100; i++) {
			futures.add(client.getJson(UriBuilder.fromPath("/generate").queryParam("num", 10).queryParam("max", 10).toTemplate(), RandomNumberResponse.class));
		}
		
		long end = new Date().getTime();
		System.out.println("Took " + (end - start) + " milliseconds to Gather Futures");
		int i = 0;
		for (Future<RandomNumberResponse> future : futures) {
			System.out.println(++i + " " + Await.result(future, Duration.forever()));
		}
		end = new Date().getTime();
		System.out.println("Took " + (end - start) + " milliseconds to print Futures");
		
		server.close();
		client.close();
	}
}
