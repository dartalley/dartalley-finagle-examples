package com.dartalley.finagle.examples.simplejson;

import java.util.List;
import java.util.Random;

import com.dartalley.finagle.examples.simplejson.RandomNumberHandler.RandomNumberRequest;
import com.dartalley.finagle.examples.simplejson.RandomNumberHandler.RandomNumberResponse;
import com.dartalley.finagle.handlers.Handler;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.Lists;

public class RandomNumberHandler implements Handler<RandomNumberRequest, RandomNumberResponse> {
	Random rand = new Random();
	
	@Override
	public RandomNumberResponse handle(RandomNumberRequest request) throws Exception {
		List<Integer> numbers = Lists.newArrayList();
		int num = request.getNum();
		int max = request.getMax();
		for (int i = 0; i < num; i++) {
			numbers.add(rand.nextInt(max));
		}
		
		Thread.sleep(500);  // Lets pretend this takes a long time so we can demonstrate non blocking
		return new RandomNumberResponse(numbers);
	}
	
	public static class RandomNumberRequest {
		private final int num;
		private final int max;
		public RandomNumberRequest(
				@JsonProperty("num") int num,
				@JsonProperty("max") int max) {
			super();
			this.num = num;
			this.max = max;
		}
		public int getNum() {
			return num;
		}
		public int getMax() {
			return max;
		}
	}
	
	public static class RandomNumberResponse {
		private final List<Integer> numbers;

		public RandomNumberResponse(@JsonProperty("numbers") List<Integer> numbers) {
			super();
			this.numbers = numbers;
		}

		public List<Integer> getNumbers() {
			return numbers;
		}

		@Override
		public String toString() {
			return "RandomNumberResponse [numbers=" + numbers + "]";
		}
	}
}
