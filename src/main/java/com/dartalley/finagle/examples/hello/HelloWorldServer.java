package com.dartalley.finagle.examples.hello;

import org.jboss.netty.handler.codec.http.HttpResponseStatus;

import com.dartalley.common.server.finagle.FinagleServerBuilder;
import com.dartalley.common.server.finagle.ServiceBuilder;
import com.dartalley.finagle.router.EndPoints;

public class HelloWorldServer {
	public static void main(String[] args) {
		ServiceBuilder sb = ServiceBuilder.create();
		FinagleServerBuilder.create()
			.bind(EndPoints.get("/").build(), sb.text(new HelloWorldHandler("Hello World"), HttpResponseStatus.OK))
			.build();
	}
}
