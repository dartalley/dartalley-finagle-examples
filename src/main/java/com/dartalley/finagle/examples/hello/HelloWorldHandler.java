package com.dartalley.finagle.examples.hello;

import com.dartalley.finagle.handlers.Handler;
import com.twitter.finagle.http.Request;

public class HelloWorldHandler implements Handler<Request, String> {
	private final String message;
	
	public HelloWorldHandler(String message) {
		this.message = message;
	}
	
	@Override
	public String handle(Request request) throws Exception {
		String name = request.getParam("name");
		if (null != name && name.isEmpty() == false) {
			return name + ": " + message;
		}
		return message;
	}
}
